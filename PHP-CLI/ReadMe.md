Some notes about WHY there are multiple similar files here.

"test-http-methods-1.php" - Uses PHP's networking "fsockopen" function,

"test-http-methods-2.php" - Uses PHP's cURL library functions,

"test-http-methods-3.php" - Uses PHP's Stream library and some filesystem functions.

Each one has its own requirements documented in the comment section of the source code.

Otherwise,

The basic commandline format is the same for all of them!


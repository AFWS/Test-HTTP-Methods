#!/usr/bin/env php
<?php
/*
 *	APPLICATION TITLE: HTTP-Methods Tester
 *  PURPOSE: Test a server for which HTTP-methods are usable.
 *	AUTHOR: Jim S. Smith
 *	COPYRIGHT: (c)2022 - A FRESH WEB SOLUTION, Licensed, with attribution, under GPL 2.0 or later.
 *
 *	VERSION: 1.0.1c, Updated: 2022/10/02
 *
 *  DESCRIPTION: Use this tool to test a web-server for available HTTP methods enabled.
 *  It will report back the server return codes for each method tried.
 *
 *   A useful PHP script that uses PHP Stream functions in order to connect
 *       to a remote web server, and to discover which HTTP methods are supported/blocked.
 *
 *       You can see this below in the source code:
 *
 *   (HTTP 1.0 - GET,HEAD,POST),
 *   (HTTP 1.1 + CONNECT,COPY,DEBUG,DELETE,LOCK,MKCOL,MOVE,OPTIONS,PATCH,PROPFIND,PROPPATCH,PUT,TRACE,TRACK,UNLOCK)
 *
 *  - This application was developed as a "concept idea", and not intended for real-world uses!
 *
 *  Therefore,
 *
 *  All responsibility from results of the use of this application is entirely YOURS!
 *
 *  - USE AT YOUR OWN RISK!
 *
 *  REQUIREMENTS:
 *
 *  1. PHP version of at least 7.3 or greater.
 *  2. PHP Stream library(ies) installed and activated.
 *  3. The PHP.INI setting: "allow_url_fopen" must be enabled (IE: through "allow_url_fopen = On").
 *
 */

/* *     APPLICATION INFO.  * */
define( 'APPLICATION', 'HTTP-Methods Tester' );
define( 'VERSION', '1.0.1c' );
define( 'RELEASE_DATE', date( 'Y-m-d', filemtime( __FILE__ ) ) );
define( 'AUTHOR', 'Jim S. Smith' );
define( 'COPYRIGHT', 'A FRESH WEB SOLUTION' );
define( 'DESCRIPTION', 'Use this tool to test a web-server for available HTTP methods enabled.' );

/** OTHER DEFINES AND VARIABLES.    **/
define( 'CHARSET', @ini_get( 'default_charset' ) );

$this_UA = 'PHP-CLI SCRIPT/' . APPLICATION . '(using Stream-lib); V' . VERSION . '; (c)2022 AFWS';    //  Use THIS User-Agent String.
$this_REF = '/' . str_replace( ' ', '-', APPLICATION ) . '/AFWS';    //  Use THIS as the Referer.

$connect_time = 5;  //  In seconds.


/** DEFINED FUNCTIONS.  **/
function get_this_uid() {
    ob_start();
    $this_uid = system( 'id -u' );
    ob_end_clean();

    return $this_uid;
}

function post_errors( $text_data = 'No operation' ) {
    echo "ERROR: $text_data!\n\n";
}


function show_usage() {
    $THIS_APP = basename( __FILE__ );

    die( <<< _HELP_TEXT_
TEST A HOST SERVER FOR AVAILABLE HTTP METHODS (AKA: "verbs").

(Uses PHP's Stream functions with other filesystem functions.)

= = = = = = = = = = = = = = = = = = = = = = = = =
USAGE: $THIS_APP -s <host name> -p <opt:port> -v <opt:proto ver>

The "-s", or "server" argument, at minimum, is mandatory.

Type: $THIS_APP -h to see more info.


{$GLOBALS['clause']}


_HELP_TEXT_
    );
}

function show_help() {
    $THIS_APP = basename( __FILE__ );
    $VERSION = VERSION;
    $THIS_TITLE = strtoupper( $THIS_APP );

    die( <<< _HELP_TEXT_
$THIS_TITLE, Version: $VERSION

$THIS_APP - Is used to test a host server to see which HTTP methods (or "verbs") are supported.
The mode of operation is simple, and only reports the abbreviated status header line.

EXAMPLE: <sudo>? $THIS_APP -s https://my-example.com -p 8080 -v 1.1

( -h for help, anywhere, will stop execution and display this help text. )

FLAGS/OPTIONS:

 !  -s/server   Server HOST-address (FQDN with or without the preceding "http://" or "https://").

    -p/port     Port number (usually will be either '80' [default] or '443'.)

    -v/version  HTTP Protocol Version ('1.0', '1.1', or '2')

    -h/help     Help - Show this screen.

NOTE:

"sudo $THIS_APP" in order to test port ranges below '1000' other than just '80' or '443'.
Non-root users are limited to these minimum port address ranges, with a total maximum of '65535'
  for ALL users.

The "-s", or "server" argument, at minimum, is mandatory.


{$GLOBALS['clause']}


_HELP_TEXT_
    );
}

$clause = <<< _TEXT_
DISCLAIMER/INDEMNITY CLAUSE:
=================================================================================
ONLY TEST ON SERVERS YOU HAVE EXPLICIT PERMISSION TO DO SO! ! !

    Any such unauthorized "testing" may be interpreted as trespassing,
    and can be charged as a criminal offense to the offending user(s).

    I assume absolutely NO responsibility for how this script is used by third
    parties. It is only provided here as an educational tool, as well as to
    encourage only properly authorized uses for security-related testing and
    evaluation of any system it may be used on.
_TEXT_;

//  Get the time, in microseconds.
function microtime_float() {
    list( $usec, $sec ) = explode( " ", microtime() );
    return ( (float)$usec + (float)$sec );
}

function only_one( $opt_data = [] ) {
	if ( is_array( $opt_data ) ) {
		return $opt_data[0];
		}

	return $opt_data;
}

function send_a_header( $test_host, $test_port, $this_method ) {
	global $connect_time, $this_UA, $this_REF, $Proto_Version, $these_Protos;

	if ( preg_match( '#^(https?)://#', $test_host, $m ) ) {
		$proto = $m[1];
		}

	else {
		$proto = 'http';
		}

//    $test_host = preg_replace( "#^({$proto}://)#", '', $test_host );
	$this_method = trim( $this_method );

//	$the_data = 'test-value-1=This is a test&test-value-2=This is my script!'; //die("DATA: $the_data\n\n");

//	Create a new Stream resource
	$opts = [
		'http' => [
			'protocol_version'	=> (float)$Proto_Version,
			'timeout'			=> (float)$connect_time,
			'method'			=> $this_method,
			'ignore_errors'		=> true,

			'header'			=> implode( "\r\n", [

									"User-Agent: $this_UA",
									"Referer: $this_REF",

									] ),

//		'content'				=> urlencode( $the_data ),
			],
		];

	$context = stream_context_create( $opts );

/*	Sends an http request to URL with additional headers shown above.	*/
	if ( $fp = fopen( "$test_host", 'r', false, $context ) ) {
		$out = implode( "\r\n", stream_get_meta_data( $fp )['wrapper_data'] ) . "\r\n";
		fclose( $fp );
		}

	elseif ( ! ( $out = file_get_contents( "$test_host", false, $context ) ) ) {
		return false;
		}

//		We only need the Response Header, not the Response Body as well.
//		return trim( htmlspecialchars( strstr( $out, "\r\n\r\n", true ), ENT_QUOTES, CHARSET ) );

//	Only return the first line of the Response Header with the server-response code.
	return trim( strstr( $out, "\r\n", true ) );
}

function send_the_request( $test_host, $test_port, $line_item ) {
    $out = send_a_header( $test_host, $test_port, $line_item );

    if ( $out && preg_match( '#^HTTP/(1\.\d|2|3)\s(\d{3})\ .*#', $out, $m ) ) $return_code = $m[2];

    else {
        $return_code = '???';
        $out = 'No data.';
        }

//  Follow server "300" redirects.
//
//  if ( (int)$return_code >= 300 || (int)$return_code <= 399 && preg_match( '#Location:\ (.*)\r\n#i', $out, $m ) ) {
//      $out = send_a_header( trim( $m[1] ), $test_port, $line_item );
//      }
//
//  if ( $out && preg_match( '#^HTTP/(1\.\d|2|3)\s(2\d\d|30\d)\ .*#', $out, $m ) ) $return_code = $m[2];
//
//  else {
//      $return_code = '???';
//      $out = 'No data.';
//      }

    $text_out = "$out\n";

    sleep( (int)$GLOBALS['Delay'] );

    return $text_out;
}

function send_multiple_requests( $test_host, $test_port, $methods = [] ) {
    if ( is_array( $methods ) && count( $methods ) ) {

        foreach ( $methods as $this_method ) {
            echo substr( "$this_method :" . str_repeat( '_', 32 ), 0, 32 ) . send_the_request( $test_host, $test_port, $this_method );
            }

        return true;
        }

    else {
        return false;
        }
}


/** START OF MAIN EXECUTION.    **/

if ( count( $argv ) < 2 ) {
	post_errors( "No arguments specified" );
    show_usage();
    }


//  Default values.
$this_HOST = '';
$port = '';
$Proto_Version = '1.1';
$Delay = 1;

//  Set lowest testable port according to user's privilege level.
$lower = ( get_this_uid() ? 1000 : 0 );

//	Command-line options parsing.
$theseOptions = getopt( 's:p:v:h', [ 'server:', 'port:', 'version:', 'help' ] );

if ( ! is_array( $theseOptions ) ) {
    exit( 22 );
    }

foreach ( $theseOptions as $thisOpt => $optValue ) {

//  Protect against repeated iterations of any options (first iteration used, only).
/*
    if ( is_array( $optValue ) ) {
        post_errors( "Option '$thisOpt' was repeated! Only first instance will be used." );
        $optValue = $optValue[0];
        }
*/
	$optValue = only_one( $optValue );

    switch ( $thisOpt ) {

        case 'server':
        case 's':
            if ( ! preg_match( '#^(https?://)?[a-z0-9-.]+$#', $optValue ) ) {
                post_errors( "Server host argument, '$optValue', is invalid" );
                exit( 9 );
                }

            $this_HOST = $optValue;
        break;

        case 'port':
        case 'p':
            if ( preg_match( '#[^0-9]+#', $optValue ) ) {
                post_errors( "Port argument MUST be an integer" );
                exit( 9 );
                }

            if ( $optValue != 80 && $optValue != 443 && $optValue < $lower || $optValue > 65535 ) {
                post_errors( "Port argument, ($optValue), is outside the usable range: $lower - 65535 for this user" );
                exit( 9 );
                }

            $port = $optValue;
        break;

        case 'version':
        case 'v':
            if ( ! in_array( $optValue, $these_Protos ) ) {
                post_errors( "Invalid HTTP protocol version, '$optValue'" );
                exit( 9 );
                }

            $Proto_Version = "HTTP/$optValue";
        break;

        case 'help':
        case 'h':
            show_help();
        break;

//   PHP's "getopt()"-implementation does NOT trap out invalid options. Boo-hoo!
/*
		case '':
        default :
            post_errors( 'Unrecognized option used!' );
            show_usage();
            exit( 22 );
*/
        }
    }

//	MUST have a server host specified!
if ( empty( $this_HOST ) ) {
    post_errors( "Server-host can not be empty" );
    exit( 9 );
    }


//	Make sure we have the protocol AND a valid port number.
if ( preg_match( '#^(https://)#', $this_HOST ) && empty( $port ) ) {
    $port = 443;
    }

elseif ( preg_match( '#^(http://)#', $this_HOST ) && empty( $port ) ) {
    $port = 80;
    }

elseif ( ! preg_match( '#^(https?://)#', $this_HOST ) ) {
    if ( empty( $port ) ) {
        $port = 80;
        $this_HOST = "http://$this_HOST";
        }

    elseif ( $port == 443 ) {
        $this_HOST = "https://$this_HOST";
        }

    else {
        $this_HOST = "http://$this_HOST";
        }
    }


/**	BEGIN THE TEST REQUESTS.	**/

echo "TESTING HOST: '$this_HOST' ON PORT: '$port' USING: 'HTTP/$Proto_Version'\n";
echo "--------------------------------------------------------------------\n\n";


//#   HTTP/1.0 standard methods.
$HTTP_1_Methods = [ 'HEAD', 'GET', 'POST', 'OPTIONS' ];

echo "TESTING HTTP/1.0 METHODS:\n";
echo "--------------------------------------------------------------------\n";
send_multiple_requests( $this_HOST, $port, $HTTP_1_Methods );
echo "\n\n";


//#   "OPTIONS" variants.
//$Option_Tests = [ 'OPTIONS *', 'OPTIONS /', 'OPTIONS /*', 'OPTIONS */*', 'OPTIONS **' ];
/*
echo "TESTING 'OPTIONS' VARIANTS:\n";
echo "--------------------------------------------------------------------\n";
send_multiple_requests( $this_HOST, $port, $Option_Tests );
echo "\n\n";
*/


//#   HTTP/1.1 added methods.
$HTTP_plus_Methods = [ 'BASELINE_CONTROL', 'CHECKIN', 'CHECKOUT', 'CONNECT', 'COPY', 'DEBUG', 'DELETE', 'INVALID', 'LABEL', 'LOCK', 'MERGE', 'MKACTIVITY', 'MKCOL', 'MKWORKSPACE', 'MOVE', 'PATCH', 'PROPFIND', 'PROPPATCH', 'PUT', 'REPORT', 'TRACE', 'TRACK', 'UNCHECKOUT', 'UNLOCK', 'UPDATE', 'VIEW', 'VERSION_CONTROL' ];

echo "TESTING HTTP/1.1 METHODS:\n";
echo "--------------------------------------------------------------------\n";
send_multiple_requests( $this_HOST, $port, $HTTP_plus_Methods );
echo "\n\n";


//#   Faked/phony "methods".
$Fake_Methods = [ 'ABCDEFGHIJ', 'TRY-ME', 'MYTEST', 'A-JOKE-RIGHT', 'FAULTY', 'FAKE', 'JAZZ_MAN', 'ZZZ', str_shuffle( 'AaBbCcDdEeFfGg-' ), 'POTS', 'STOP', 'SPOT', 'HADE', 'CLOCK', 'CRATE' ];
shuffle( $Fake_Methods );

echo "TESTING ARBITRARY/FAKE METHODS:\n";
echo "--------------------------------------------------------------------\n";
send_multiple_requests( $this_HOST, $port, $Fake_Methods );
echo "\n\n";


// - THIS SECTION FOR ADVANCED TESTING! - //

//#   Malformed "methods".
/*
$Malformed = [ '\n', '\r\n', '\0', '*', ';', chr( 0 ), 0, "\%7e", chr( 13 ), chr( 13 ) . chr( 10 ), '`', chr( 13 ) . 'HEAD;' . chr( 0 ), chr( 34 ), '" ; {0}' . chr( 13 ) . chr( 10 ) . '|' . chr( 0 ) . '""', chr( 9 ), '`ls -al`; aa ' . chr( 10 ) . 'HEAD / HTTP/2 ~' . chr( 10 ) . '"' ];
shuffle( $Malformed );

echo "TESTING MALFORMED METHODS:\n";
echo "--------------------------------------------------------------------\n";
send_multiple_requests( $this_HOST, $port, $Malformed );
echo "\n\n";
*/

echo "DONE!\n\n";

echo "$clause\n\n";


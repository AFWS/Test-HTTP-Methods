# Test-HTTP-Methods

Some scripts to test for usable HTTP-methods on a server.

 *	VERSION: 1.0.1c, Updated: 2022/10/02
 *
 *  DESCRIPTION: Use this tool to test a web-server for available HTTP methods enabled.
 *  It will report back the server return codes for each method tried.
 *
 *   Some usefuls scripts that connect to a remote web server and discover which HTTP methods are supported/blocked.
 *
 *   (HTTP 1.0 - GET,HEAD,POST),
 *   (HTTP 1.1 + CONNECT,COPY,DEBUG,DELETE,LOCK,MKCOL,MOVE,OPTIONS,PATCH,PROPFIND,PROPPATCH,PUT,TRACE,TRACK,UNLOCK)
 *
 *  - This application was developed as a "concept idea", and not intended for real-world uses!
 *
 *  Therefore,
 *
 *  All responsibility from results of the use of this application is entirely YOURS!
 *
 *  - USE AT YOUR OWN RISK!


 DISCLAIMER/INDEMNITY CLAUSE:
 =================================================================================
 **ONLY TEST ON SERVERS YOU HAVE EXPLICIT PERMISSION TO DO SO! ! !**

    Any such unauthorized "testing" may be interpreted as trespassing,
    and can be charged as a criminal offense to the offending user(s).

    I assume absolutely NO responsibility for how this script is used by third
    parties. It is only provided here as an educational tool, as well as to
    encourage only properly authorized uses for security-related testing and
    evaluation of any system it may be used on.

